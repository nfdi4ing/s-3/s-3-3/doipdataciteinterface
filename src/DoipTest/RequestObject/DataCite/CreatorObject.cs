﻿namespace Coscine.Api.DoipTest.RequestObject.DataCite
{
    /// <summary>
    /// CreatorObject
    /// </summary>
    public class CreatorObject
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
