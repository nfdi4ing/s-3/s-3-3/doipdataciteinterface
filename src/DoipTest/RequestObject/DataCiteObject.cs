﻿using Coscine.Api.DoipTest.RequestObject.DataCite;
using System.Collections.Generic;

namespace Coscine.Api.DoipTest.RequestObject
{
    /// <summary>
    /// DataCiteObject
    /// </summary>
    public class DataCiteObject
    {
        /// <summary>
        /// Identifiers
        /// </summary>
        public IEnumerable<IdentifierObject> Identifiers { get; set; }
        /// <summary>
        /// Creators
        /// </summary>
        public IEnumerable<CreatorObject> Creators { get; set; }
        /// <summary>
        /// Titles
        /// </summary>
        public IEnumerable<TitleObject> Titles { get; set; }
        /// <summary>
        /// Publisher
        /// </summary>
        public string Publisher { get; set; }
        /// <summary>
        /// PublicationYear
        /// </summary>
        public string PublicationYear { get; set; }
        /// <summary>
        /// Formats
        /// 
        /// Possible values:
        /// - application/ld+json
        /// - application/json
        /// - application/xml
        /// </summary>
        public IEnumerable<string> Formats { get; set; }
        /// <summary>
        /// Types
        /// </summary>
        public IEnumerable<TypeObject> Types { get; set; }
        /// <summary>
        /// RelatedIdentifiers
        /// </summary>
        public IEnumerable<RelatedIdentifierObject> RelatedIdentifiers { get; set; }
        /// <summary>
        /// SchemaVersion
        /// </summary>
        public string SchemaVersion { get; set; }
    }
}
