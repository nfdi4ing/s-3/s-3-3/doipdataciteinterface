﻿namespace Coscine.Api.DoipTest.ResponseObject
{
    /// <summary>
    /// DoipResponseObject
    /// </summary>
    public class DoipResponseObject
    {
        /// <summary>
        /// RequestId
        /// </summary>
        public string RequestId;
        /// <summary>
        /// Status
        /// </summary>
        public string Status;
        /// <summary>
        /// Attributes
        /// </summary>
        public string[] Attributes;
        /// <summary>
        /// Output
        /// </summary>
        public object Output;
    }
}
