﻿namespace Coscine.Api.DoipTest.RequestObject
{
    /// <summary>
    /// Digital Object
    /// </summary>
    public class DigitalObject
    {
        /// <summary>
        /// Definition
        /// </summary>
        public string Definition { get; set; }
    }
}
