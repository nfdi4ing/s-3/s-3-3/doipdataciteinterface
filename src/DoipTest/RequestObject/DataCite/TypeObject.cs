﻿namespace Coscine.Api.DoipTest.RequestObject.DataCite
{
    /// <summary>
    /// TypeObject
    /// </summary>
    public class TypeObject
    {
        /// <summary>
        /// ResourceTypeGeneral
        /// </summary>
        public string ResourceTypeGeneral { get; set; }
        /// <summary>
        /// ResourceType
        /// </summary>
        public string ResourceType { get; set; }
    }
}
