﻿namespace Coscine.Api.DoipTest.RequestObject
{
    /// <summary>
    /// DoipRequestObject
    /// </summary>
    public class DoipRequestObject
    {
        /// <summary>
        /// RequestId
        /// </summary>
        public string RequestId;
        /// <summary>
        /// ClientId
        /// </summary>
        public string ClientId;
        /// <summary>
        /// TargetId
        /// </summary>
        public string TargetId;
        /// <summary>
        /// OperationId
        /// </summary>
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public Operation OperationId;
        /// <summary>
        /// Attributes
        /// </summary>
        public string[] Attributes;
        /// <summary>
        /// Authentification
        /// </summary>
        public string Authentification;
        /// <summary>
        /// Input
        /// </summary>
        public Input Input;
    }
}
