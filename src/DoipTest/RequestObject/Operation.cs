﻿namespace Coscine.Api.DoipTest.RequestObject
{
    /// <summary>
    /// Operation
    /// </summary>
    public enum Operation
    {
        /// <summary>
        /// Hello Operation "0.DOIP/Op.Hello"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.Hello")]
        Hello,
        /// <summary>
        /// Create Operation "0.DOIP/Op.Create"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.Create")]
        Create,
        /// <summary>
        /// Retrieve Operation "0.DOIP/Op.Retrieve"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.Retrieve")]
        Retrieve,
        /// <summary>
        /// Update Operation "0.DOIP/Op.Update"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.Update")]
        Update,
        /// <summary>
        /// Delete Operation "0.DOIP/Op.Delete"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.Delete")]
        Delete,
        /// <summary>
        /// Search Operation "0.DOIP/Op.Search"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.Search")]
        Search,
        /// <summary>
        /// ListOperations Operation "0.DOIP/Op.ListOperations"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.ListOperations")]
        ListOperations,
        /// <summary>
        /// Validate Operation "0.DOIP/Op.Validate"
        /// </summary>
        [System.Runtime.Serialization.EnumMember(Value = @"0.DOIP/Op.Validate")]
        Validate,
    }
}
