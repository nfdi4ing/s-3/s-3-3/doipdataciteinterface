﻿namespace Coscine.Api.DoipTest.RequestObject
{
    /// <summary>
    /// Input
    /// </summary>
    public class Input
    {
        /// <summary>
        /// Metadata
        /// </summary>
        public DataCiteObject Metadata { get; set; }
        /// <summary>
        /// Document
        /// </summary>
        public DigitalObject Document { get; set; }
    }
}
