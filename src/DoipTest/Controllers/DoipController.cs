﻿using Coscine.Api.DoipTest.RequestObject;
using Coscine.Api.DoipTest.ResponseObject;
using Coscine.Configuration;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Coscine.Api.DoipTest.Controllers
{
    /// <summary>
    /// Controller for Doip Interaction.
    /// </summary>
    public class DoipController : Controller
    {
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Default constructor with initilization.
        /// </summary>
        public DoipController()
        {
            _configuration = Program.Configuration;
        }

        /// <summary>
        /// HandleDoipRequest
        /// </summary>
        /// <param name="request">List of DoipRequestObjects</param>
        /// <returns>List of DoipResponseObjects</returns>
        [HttpPost("[controller]")]
        public ActionResult<IEnumerable<DoipResponseObject>> HandleDoipRequest([FromBody] IEnumerable<DoipRequestObject> request)
        {
            var responseObjects = new List<DoipResponseObject>();
            foreach (var requestObject in request)
            {
                switch (requestObject.OperationId)
                {
                    case Operation.Hello:
                        responseObjects.Add(new DoipResponseObject()
                        {
                            Attributes = requestObject.Attributes,
                            Status = "0.DOIP/Status.200",
                            Output = "hello",
                            RequestId = requestObject.RequestId
                        });
                        break;
                    case Operation.Validate:
                        responseObjects.Add(new DoipResponseObject()
                        {
                            Attributes = requestObject.Attributes,
                            Status = "0.DOIP/Status.200",
                            Output = "validate",
                            RequestId = requestObject.RequestId
                        });
                        break;
                    default:
                        responseObjects.Add(new DoipResponseObject()
                        {
                            Attributes = requestObject.Attributes,
                            Status = "0.DOIP/Status.200",
                            Output = requestObject.Input,
                            RequestId = requestObject.RequestId
                        });
                        break;
                }

            }
            return Ok(responseObjects);
        }

    }
}
