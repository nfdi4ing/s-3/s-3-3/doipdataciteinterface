﻿namespace Coscine.Api.DoipTest.RequestObject.DataCite
{
    /// <summary>
    /// RelatedIdentifierObject
    /// </summary>
    public class RelatedIdentifierObject
    {
        /// <summary>
        /// RelatedIdentifier
        /// </summary>
        public string RelatedIdentifier { get; set; }
        /// <summary>
        /// RelatedIdentifierType
        /// </summary>
        public string RelatedIdentifierType { get; set; }
        /// <summary>
        /// RelationType
        /// </summary>
        public string RelationType { get; set; }
    }
}
