﻿namespace Coscine.Api.DoipTest.RequestObject.DataCite
{
    /// <summary>
    /// IdentifierObject
    /// </summary>
    public class IdentifierObject
    {
        /// <summary>
        /// IdentifierType
        /// </summary>
        public string IdentifierType { get; set; }
        /// <summary>
        /// Identifier
        /// </summary>
        public string Identifier { get; set; }
    }
}
