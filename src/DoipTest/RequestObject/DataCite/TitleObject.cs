﻿namespace Coscine.Api.DoipTest.RequestObject.DataCite
{
    /// <summary>
    /// TitleObject
    /// </summary>
    public class TitleObject
    {
        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }
    }
}
